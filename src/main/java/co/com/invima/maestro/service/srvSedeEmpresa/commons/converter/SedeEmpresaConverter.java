package co.com.invima.maestro.service.srvSedeEmpresa.commons.converter;

import co.com.invima.maestro.modeloTransversal.dto.sedeEmpresa.EmpresaSedeDTO;
import co.com.invima.maestro.modeloTransversal.entities.sedeEmpresa.EmpresaSedeDAO;
import javassist.NotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class SedeEmpresaConverter {


    /**
     * @param empresaSedeDAO
     * @return empresaSedeDTO
     * @author JBermeo
     * method to convert EmpresaSedeDAO to EmpresaSedeDTO
     */
    public EmpresaSedeDTO empresaSedeDAOtoDTO(EmpresaSedeDAO empresaSedeDAO, ModelMapper modelMapper) {
        EmpresaSedeDTO empresaSedeDTO = new EmpresaSedeDTO();
        modelMapper.map(empresaSedeDAO, empresaSedeDTO);
        return empresaSedeDTO;

    }


    /**
     * @param empresaSedeDTO
     * @return empresaSedeDAO
     * @author JBermeo
     * method to convert EmpresaSedeDTO to EmpresaSedeDAO
     */
    public EmpresaSedeDAO empresaSedeDTOtoDAO(EmpresaSedeDTO empresaSedeDTO,
                                              ModelMapper modelMapper) throws NotFoundException {
        EmpresaSedeDAO empresaSedeDAO = new EmpresaSedeDAO();
        modelMapper.map(empresaSedeDTO, empresaSedeDAO);

        return empresaSedeDAO;
    }
}
