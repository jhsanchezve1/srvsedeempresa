package co.com.invima.maestro.service.srvSedeEmpresa.repository;


import co.com.invima.maestro.modeloTransversal.entities.actividadEconomica.ActividadEconomicaDAO;
import co.com.invima.maestro.modeloTransversal.entities.sedeEmpresa.EmpresaSedeDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.query.Procedure;

import java.util.List;
import java.util.Optional;

@Repository
public interface IEmpresaSedeRepository extends JpaRepository<EmpresaSedeDAO, Integer>, JpaSpecificationExecutor<EmpresaSedeDAO> {

    @Query("select empresaSede from EmpresaSedeDAO empresaSede where empresaSede.activo=true")
    List<EmpresaSedeDAO> consultarEmpresaSedes();

    @Query("select empresaSede from EmpresaSedeDAO empresaSede where empresaSede.id=:codigo" +
            " and empresaSede.activo=true")
    EmpresaSedeDAO consultarPorId(@Param("codigo") Integer codigo);

    @Query("select actividad from ActividadEconomicaDAO actividad where actividad.id=:codigo" +
            " and actividad.activo=true")
    Optional<ActividadEconomicaDAO> consultarActividadPorId(@Param("codigo") Integer codigo);

    @Procedure("dbo.USP_SedeEmpresa_U")
    String editarSedeEmpresa(String json);

    @Procedure("dbo.USP_Sede_I")
    String crearSedeEmpresa(String json);

    @Procedure("dbo.USP_Direccion_U")
    String actualizarDireccion(String json);

    @Procedure("dbo.USP_SedeEmpresa_S")
    String consultarSedeEmpresa(Integer idSede);

    @Procedure("dbo.USP_Sede_S")
    String consultarSedeEmpresaPorId(Integer idSede);

}
