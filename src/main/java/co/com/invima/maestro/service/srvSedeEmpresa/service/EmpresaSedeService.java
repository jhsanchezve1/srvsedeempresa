package co.com.invima.maestro.service.srvSedeEmpresa.service;

import co.com.invima.maestro.modeloTransversal.dto.generic.GenericRequestDTO;
import co.com.invima.maestro.modeloTransversal.dto.generic.GenericResponseDTO;
import co.com.invima.maestro.modeloTransversal.dto.sedeEmpresa.EmpresaSedeDTO;
import co.com.invima.maestro.modeloTransversal.entities.actividadEconomica.ActividadEconomicaDAO;
import co.com.invima.maestro.modeloTransversal.entities.sedeEmpresa.EmpresaSedeDAO;
import co.com.invima.maestro.service.srvSedeEmpresa.commons.converter.SedeEmpresaConverter;
import co.com.invima.maestro.service.srvSedeEmpresa.repository.IEmpresaSedeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import java.util.List;
import java.util.Optional;

@Service
public class EmpresaSedeService implements IEmpresaSedeService {


    private final IEmpresaSedeRepository iEmpresaSedeRepository;
    private final ModelMapper modelMapper;

    private final SedeEmpresaConverter sedeEmpresaConverter;

    ObjectMapper objectMapper = new ObjectMapper();

    private static final String MENSAJE = "mensaje";
    private static final String RESPUESTA = "respuesta";
    private static final String CODIGO = "codigo";
    
    

    @Autowired
    public EmpresaSedeService(SedeEmpresaConverter sedeEmpresaConverter, ModelMapper modelMapper, IEmpresaSedeRepository iEmpresaSedeRepository) {

        this.sedeEmpresaConverter = sedeEmpresaConverter;
        this.modelMapper = modelMapper;
        this.iEmpresaSedeRepository = iEmpresaSedeRepository;
    }

    @Override
    public ResponseEntity<GenericResponseDTO> consultarPorId(Integer idSede) {
        try {

            String respuestaTramite = iEmpresaSedeRepository.consultarSedeEmpresaPorId(idSede);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(respuestaTramite);

            JSONObject respuestGenerica = (JSONObject) json.get(RESPUESTA);
            String codigo = (String) respuestGenerica.get(CODIGO);

            if (codigo.equals("00")) {
                JSONArray mensaje1 = (JSONArray) json.get("sede");
                JSONObject mensaje = (JSONObject) mensaje1.get(0);
                return new ResponseEntity<>(GenericResponseDTO.builder().message("Se consulta la sede").objectResponse(mensaje)
                        .statusCode(HttpStatus.OK.value()).build(), HttpStatus.OK);
            } else {
                String descripcion = (String) respuestGenerica.get(MENSAJE);
                return new ResponseEntity<>(GenericResponseDTO.builder().message(descripcion)
                        .objectResponse(new JSONObject()).statusCode(HttpStatus.CONFLICT.value()).build(),
                        HttpStatus.CONFLICT);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(GenericResponseDTO.builder().message("Error consultando sede:  " + e.getMessage())
                    .objectResponse(false).statusCode(HttpStatus.BAD_REQUEST.value()).build(), HttpStatus.BAD_REQUEST);
        }
    }



    @Override
    public ResponseEntity<GenericResponseDTO> consultarEmpresaSedes() {
        try {
            List<EmpresaSedeDAO> list = iEmpresaSedeRepository.consultarEmpresaSedes();

            return new ResponseEntity<>(GenericResponseDTO.builder().message("Se consultan las empresas sedes")
                    .objectResponse(list).statusCode(HttpStatus.OK.value()).build(), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(GenericResponseDTO.builder().message("Error consultando las empresas sedes:  " + e.getMessage())
                    .objectResponse(null).statusCode(HttpStatus.BAD_REQUEST.value()).build(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResponseDTO> crear(GenericRequestDTO genericRequestDTO) {
        try {
            EmpresaSedeDTO empresaSedeDTO = new EmpresaSedeDTO();
            modelMapper.map(genericRequestDTO.getRequest(), empresaSedeDTO);
            EmpresaSedeDAO empresaSedeDAO = sedeEmpresaConverter.empresaSedeDTOtoDAO(empresaSedeDTO, modelMapper);
            empresaSedeDAO.setActivo(true);
            //Integer inscripcion = empresaSedeDAO.getCodigoInscripcion().length();
            //String inscri = empresaSedeDAO.getCodigoInscripcion().substring(0,inscripcion-2);
            //Integer codigo = iEmpresaSedeRepository.consultarPorNumeroDocumento();
            iEmpresaSedeRepository.save(empresaSedeDAO);

            return new ResponseEntity<>(GenericResponseDTO.builder().message("Se crea la empresa sede")
                    .objectResponse(true).statusCode(HttpStatus.CREATED.value()).build(), HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(GenericResponseDTO.builder().message("Error creando la empresa sede:  " + e.getMessage())
                    .objectResponse(false).statusCode(HttpStatus.BAD_REQUEST.value()).build(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResponseDTO> actualizar(GenericRequestDTO genericRequestDTO) {
        try {
            EmpresaSedeDTO empresaSedeDTO = new EmpresaSedeDTO();
            modelMapper.map(genericRequestDTO.getRequest(), empresaSedeDTO);
            EmpresaSedeDAO empresaSedeDAO = sedeEmpresaConverter.empresaSedeDTOtoDAO(empresaSedeDTO, modelMapper);
            empresaSedeDAO.setActivo(true);
            iEmpresaSedeRepository.save(empresaSedeDAO);

            return new ResponseEntity<>(GenericResponseDTO.builder().message("Se actualiza la empresa sede")
                    .objectResponse(true).statusCode(HttpStatus.OK.value()).build(), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(GenericResponseDTO.builder().message("Error actualizando la empresa sede:  " + e.getMessage())
                    .objectResponse(false).statusCode(HttpStatus.BAD_REQUEST.value()).build(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResponseDTO> eliminadoLogicoPorId(Integer id) {


        Optional<EmpresaSedeDAO> empresaSedeDAO = iEmpresaSedeRepository.findById(id);

        if (!empresaSedeDAO.isPresent())
            return new ResponseEntity<>(GenericResponseDTO.builder()
                    .message("La empresa sede a eliminar no existe")
                    .objectResponse(false)
                    .statusCode(HttpStatus.CONFLICT.value())
                    .build(), HttpStatus.OK);

        empresaSedeDAO.get().setActivo(false);
        iEmpresaSedeRepository.save(empresaSedeDAO.get());

        return new ResponseEntity<>(GenericResponseDTO.builder()
                .message("Empresa sede eliminada")
                .objectResponse(true)
                .statusCode(HttpStatus.OK.value())
                .build(), HttpStatus.OK);

    }

    @Override
    public ResponseEntity<GenericResponseDTO> editarSedeEmpresa(String genericRequestDTO) {
        try {

            String respuestaTramite = iEmpresaSedeRepository.editarSedeEmpresa(genericRequestDTO);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(respuestaTramite);


            JSONObject respuestGenerica = (JSONObject) json.get(RESPUESTA);
            String codigo = (String) respuestGenerica.get(CODIGO);

            if (codigo.equals("00")) {
                JSONArray mensaje = (JSONArray) json.get(MENSAJE);
                actualizarDireccion(genericRequestDTO);
                return new ResponseEntity<>(GenericResponseDTO.builder().message("Se actualizó correctamente la sucursal").objectResponse(mensaje)
                        .statusCode(HttpStatus.OK.value()).build(), HttpStatus.OK);
            } else {
                String descripcion = (String) respuestGenerica.get("descripcion");
                return new ResponseEntity<>(GenericResponseDTO.builder().message(descripcion)
                        .objectResponse(new JSONObject()).statusCode(HttpStatus.CONFLICT.value()).build(),
                        HttpStatus.CONFLICT);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(GenericResponseDTO.builder().message("Error actualizando le sucursal:  " + e.getMessage())
                    .objectResponse(false).statusCode(HttpStatus.BAD_REQUEST.value()).build(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResponseDTO> crearSedeEmpresa(String genericRequestDTO) {
        try {

            String respuestaTramite = iEmpresaSedeRepository.crearSedeEmpresa(genericRequestDTO);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(respuestaTramite);


            JSONObject respuestGenerica = (JSONObject) json.get(RESPUESTA);
            String codigo = (String) respuestGenerica.get(CODIGO);

            if (codigo.equals("00")) {
                JSONArray mensaje = (JSONArray) json.get(MENSAJE);
                String direccion = (String) respuestGenerica.get("sede");
                actualizarDireccion(direccion.toString());
                return new ResponseEntity<>(GenericResponseDTO.builder().message("Se creó correctamente la sucursal").objectResponse(mensaje)
                        .statusCode(HttpStatus.OK.value()).build(), HttpStatus.OK);
            } else {
                String descripcion = (String) respuestGenerica.get("descripcion");
                return new ResponseEntity<>(GenericResponseDTO.builder().message(descripcion)
                        .objectResponse(new JSONObject()).statusCode(HttpStatus.CONFLICT.value()).build(),
                        HttpStatus.CONFLICT);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(GenericResponseDTO.builder().message("Error creando la sucursal:  " + e.getMessage())
                    .objectResponse(false).statusCode(HttpStatus.BAD_REQUEST.value()).build(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResponseDTO> actualizarDireccion(String genericRequestDTO) {
        try {

            String respuestaTramite = iEmpresaSedeRepository.actualizarDireccion(genericRequestDTO);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(respuestaTramite);

            JSONObject respuestGenerica = (JSONObject) json.get(RESPUESTA);
            String codigo = (String) respuestGenerica.get(CODIGO);

            if (codigo.equals("00")) {
                JSONObject mensaje = (JSONObject) json.get(MENSAJE);
                return new ResponseEntity<>(GenericResponseDTO.builder().message("Se actualiza la direccion").objectResponse(mensaje)
                        .statusCode(HttpStatus.OK.value()).build(), HttpStatus.OK);
            } else {
                String descripcion = (String) respuestGenerica.get(MENSAJE);
                return new ResponseEntity<>(GenericResponseDTO.builder().message(descripcion)
                        .objectResponse(new JSONObject()).statusCode(HttpStatus.CONFLICT.value()).build(),
                        HttpStatus.CONFLICT);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(GenericResponseDTO.builder().message("Error al actualizar la direccion:  " + e.getMessage())
                    .objectResponse(false).statusCode(HttpStatus.BAD_REQUEST.value()).build(), HttpStatus.BAD_REQUEST);
        }
    }


    @Override
    public ResponseEntity<GenericResponseDTO> consultarSedeEmpresa(Integer idSede) {
        try {

            String respuestaTramite = iEmpresaSedeRepository.consultarSedeEmpresa(idSede);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(respuestaTramite);

            JSONObject respuestGenerica = (JSONObject) json.get(RESPUESTA);
            String codigo = (String) respuestGenerica.get(CODIGO);

            if (codigo.equals("00")) {
                JSONArray mensaje = (JSONArray) json.get("sede");
                return new ResponseEntity<>(GenericResponseDTO.builder().message("Se consulta la sede").objectResponse(mensaje)
                        .statusCode(HttpStatus.OK.value()).build(), HttpStatus.OK);
            } else {
                String descripcion = (String) respuestGenerica.get(MENSAJE);
                return new ResponseEntity<>(GenericResponseDTO.builder().message(descripcion)
                        .objectResponse(new JSONObject()).statusCode(HttpStatus.CONFLICT.value()).build(),
                        HttpStatus.CONFLICT);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(GenericResponseDTO.builder().message("Error consultando sede:  " + e.getMessage())
                    .objectResponse(false).statusCode(HttpStatus.BAD_REQUEST.value()).build(), HttpStatus.BAD_REQUEST);
        }
    }
}
