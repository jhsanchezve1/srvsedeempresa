package co.com.invima.maestro.service.srvSedeEmpresa.service;


import co.com.invima.maestro.modeloTransversal.dto.generic.GenericRequestDTO;
import co.com.invima.maestro.modeloTransversal.dto.generic.GenericResponseDTO;
import org.springframework.http.ResponseEntity;


public interface IEmpresaSedeService {


    ResponseEntity<GenericResponseDTO> consultarPorId(Integer codigo);

    ResponseEntity<GenericResponseDTO> eliminadoLogicoPorId(Integer id);

    ResponseEntity<GenericResponseDTO> consultarEmpresaSedes();

    ResponseEntity<GenericResponseDTO> crear(GenericRequestDTO genericRequestDTO);

    ResponseEntity<GenericResponseDTO> actualizar(GenericRequestDTO genericRequestDTO);

    ResponseEntity<GenericResponseDTO> editarSedeEmpresa(String json);

    ResponseEntity<GenericResponseDTO> crearSedeEmpresa(String json);

    ResponseEntity<GenericResponseDTO> actualizarDireccion(String genericRequestDTO);

    ResponseEntity<GenericResponseDTO> consultarSedeEmpresa(Integer idSede);
}
