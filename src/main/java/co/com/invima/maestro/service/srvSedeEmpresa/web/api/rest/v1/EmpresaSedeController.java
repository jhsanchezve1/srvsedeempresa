package co.com.invima.maestro.service.srvSedeEmpresa.web.api.rest.v1;


import co.com.invima.maestro.modeloTransversal.dto.generic.GenericRequestDTO;
import co.com.invima.maestro.modeloTransversal.dto.generic.GenericResponseDTO;
import co.com.invima.maestro.service.srvSedeEmpresa.service.IEmpresaSedeService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/EmpresaSede")
@CrossOrigin({"*"})
public class EmpresaSedeController implements IEmpresaSedeController {

    private final IEmpresaSedeService service;

    @Autowired
    public EmpresaSedeController(IEmpresaSedeService service) {
        this.service = service;
    }


    @Override
    @GetMapping("/consultaId/{id}")
    @ApiOperation(value = "Consulta empresa sede por id", notes = "Consulta empresa sede por id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. El recurso se obtiene correctamente", response = GenericResponseDTO.class),
            @ApiResponse(code = 400, message = "Bad Request.Esta vez cambiamos el tipo de dato de la respuesta (String)", response = String.class),
            @ApiResponse(code = 500, message = "Error inesperado del sistema")})
    public ResponseEntity<GenericResponseDTO> consultarPorId(@ApiParam(type = "Integer", value =
            "el parametro usuario debe ser un json con la siguiente estructura:" +
            "<br>" +
            "{ <br> request:"
            + " {<br>"
            + "     \"id\": \"\"<br>" +
            "}<br>"

            + "      }<br>", example = "1", required = true) @PathVariable Integer id) {
        return service.consultarPorId(id);
    }

    @Override
    @GetMapping("/consulta")
    @ApiOperation(value = "Consulta las empresas sedes", notes = "Consulta las empresas sedes")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. El recurso se obtiene correctamente", response = GenericResponseDTO.class),
            @ApiResponse(code = 400, message = "Bad Request.Esta vez cambiamos el tipo de dato de la respuesta (String)", response = String.class),
            @ApiResponse(code = 500, message = "Error inesperado del sistema")})
    public ResponseEntity<GenericResponseDTO> consultarEmpresaSedes() {
        return service.consultarEmpresaSedes();
    }

    @Override
    @PostMapping("/")
    @ApiOperation(value = "Crea una nueva empresa sede", notes = "Crea una nueva empresa sede")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. El recurso se obtiene correctamente", response = GenericResponseDTO.class),
            @ApiResponse(code = 400, message = "Bad Request.Esta vez cambiamos el tipo de dato de la respuesta (String)", response = String.class),
            @ApiResponse(code = 500, message = "Error inesperado del sistema")})
    public ResponseEntity<GenericResponseDTO> crear(@ApiParam(type = "EmpresaDTO", value =
            "el parametro usuario debe ser un json con la siguiente estructura:" +
            "<br>" +
            "{ <br> request:"
            + " {<br>"
                    + "     \"celularSede\": \"\",<br>"
                    + "     \"correoElectronicoSede\": \"\",<br>"
                    + "     \"direccionSede\": \"\",<br>"
                    + "     \"telefonoSede\": \"\",<br>"
                    + "     \"idPaisSede\": ,<br>"
                    + "     \"idDepartamentoSede\": ,<br>"
                    + "     \"idMunicipioSede\": ,<br>"
                    + "     \"idActividadEconomica\": ,<br>"
                    + "     \"idEmpresa\": ,<br>"
                    + "     \"correoElectronicoSede2\": \"\",<br>"
                    + "     \"tipoSede\": ,<br>"
                    + "     \"idTipoRolEmpresa\": ,<br>"
                    + "     \"codigoInscripcion\": \"\",<br>"
                    + "     \"direccionJudicial\": \"\",<br>"
                    + "     \"direccionNotificacion\": \"\",<br>"
                    + "     \"idDireccion\": ,<br>"
                    + "     \"nombreSede\": \" \"<br>" +
            "}<br>"

            + "      }<br>", required = true) GenericRequestDTO genericRequestDTO) {
        return service.crear(genericRequestDTO);
    }

    @Override
    @PutMapping("/")
    @ApiOperation(value = "Actualiza la empresa sede", notes = "Actualiza la empresa sede")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. El recurso se obtiene correctamente", response = GenericResponseDTO.class),
            @ApiResponse(code = 400, message = "Bad Request.Esta vez cambiamos el tipo de dato de la respuesta (String)", response = String.class),
            @ApiResponse(code = 500, message = "Error inesperado del sistema")})
    public ResponseEntity<GenericResponseDTO> actualizar(@ApiParam(type = "EmpresaDTO", value =
            "el parametro usuario debe ser un json con la siguiente estructura:" +
            "<br>" +
            "{ <br> request:"
            + " {<br>"
                    + "     \"id\": \"\",<br>"
                    + "     \"celularSede\": \"\",<br>"
                    + "     \"correoElectronicoSede\": \"\",<br>"
                    + "     \"direccionSede\": \"\",<br>"
                    + "     \"telefonoSede\": \"\",<br>"
                    + "     \"idPaisSede\": ,<br>"
                    + "     \"idDepartamentoSede\": ,<br>"
                    + "     \"idMunicipioSede\": ,<br>"
                    + "     \"idActividadEconomica\": ,<br>"
                    + "     \"idEmpresa\": ,<br>"
                    + "     \"correoElectronicoSede2\": \"\",<br>"
                    + "     \"tipoSede\": ,<br>"
                    + "     \"idTipoRolEmpresa\": ,<br>"
                    + "     \"codigoInscripcion\": \"\",<br>"
                    + "     \"direccionJudicial\": \"\",<br>"
                    + "     \"direccionNotificacion\": \"\",<br>"
                    + "     \"idDireccion\": ,<br>"
                    + "     \"nombreSede\": \" \"<br>" +
            "}<br>"

            + "      }<br>", required = true) GenericRequestDTO genericRequestDTO) {
        return service.actualizar(genericRequestDTO);
    }

    @Override
    @DeleteMapping("/eliminarId/{id}")
    @ApiOperation(value = "Eliminado logico de Empresa sede por id", notes = "Eliminado logico de Empresa sede por id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. El recurso se obtiene correctamente", response = GenericResponseDTO.class),
            @ApiResponse(code = 400, message = "Bad Request.Esta vez cambiamos el tipo de dato de la respuesta (String)", response = String.class),
            @ApiResponse(code = 500, message = "Error inesperado del sistema")})
    public ResponseEntity<GenericResponseDTO> eliminadoLogicoPorId(@ApiParam(type = "Integer", value =
            "el parametro usuario debe ser un json con la siguiente estructura:" +
            "<br>" +
            "{ <br> request:"
            + " {<br>"
            + "     \"id\": \"\"<br>" +
            "}<br>"

            + "      }<br>",example = "1", required = true) @PathVariable Integer id) {
        return service.eliminadoLogicoPorId(id);
    }

    @Override
    @PutMapping("/popUpEditarSedeEmpresa")
    @ApiOperation(value = "Actualiza sede empresa", notes = "Actualiza sede empresa")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. El recurso se obtiene correctamente", response = GenericResponseDTO.class),
            @ApiResponse(code = 400, message = "Bad Request.Esta vez cambiamos el tipo de dato de la respuesta (String)", response = String.class),
            @ApiResponse(code = 500, message = "Error inesperado del sistema")})
    public ResponseEntity<GenericResponseDTO> popUpEditarSedeEmpresa(@ApiParam(value =
            "el parametro empresa debe ser un json con la siguiente estructura:" +
                    "<br> {" +

                    "    \"idSede\": 1,\n" +
                    "    \"telefonoSede\":\"12345\",\n" +
                    "    \"celularSede\": \"12345\",\n" +
                    "    \"correoElectronicoSede\": \"sede@gmail.com\",\n"
                    + "     \"estructuraDireccionComercial\": "
                    + "{\n"
                    + "     \"descripcionTipoDireccion\": \"Urbana\",\n"
                    + "     \"descripcionTipoVia\": \"Transversal\",\n"
                    + "     \"nomenclaturaDireccion\": \"E\",\n"
                    + "     \"valorNumeroNomenclatura\": 4,\n"
                    + "     \"numeroInicialDireccion\": 6,\n"
                    + "     \"numeroFinalDireccion\": 10,\n"
                    + "     \"barrio\": \"Los almendros\",\n"
                    + "     \"descripcionTipoInmueble\": \"Bodega\",\n"
                    + "     \"otroValorDescTipoInmueble\": \"\",\n"
                    + "     \"descripcionComplemento\": \"Bloque\",\n"
                    + "     \"otroValorDescComplemento\": \"\",\n"
                    + "     \"detalles\": \"\"\n"
                    + "},\n"
                    +"    \"idActividadEconomica\": 1,\n"
                    +  "}" +
                    "<br>", required = true) String json) {
        return service.editarSedeEmpresa(json);
    }

    @Override
    @PostMapping("/popUpCrearSede")
    @ApiOperation(value = "Crear sede empresa", notes = "Crear sede empresa")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. El recurso se obtiene correctamente", response = GenericResponseDTO.class),
            @ApiResponse(code = 400, message = "Bad Request.Esta vez cambiamos el tipo de dato de la respuesta (String)", response = String.class),
            @ApiResponse(code = 500, message = "Error inesperado del sistema")})
    public ResponseEntity<GenericResponseDTO> popUpCrearSede(@ApiParam(value =
            "el parametro sede empresa debe ser un json con la siguiente estructura:" +
                    "<br> {" +

                    "    \"idEmpresa\": 1,\n" +
                    "    \"nombreSede\":\"prueba\",\n" +
                    "    \"celularSede\": \"12345\",\n" +
                    "    \"correoElectronicoSede\": \"sede@gmail.com\",\n"
                    + "     \"estructuraDireccionComercial\": "
                    + "{\n"
                    + "     \"descripcionTipoDireccion\": \"Urbana\",\n"
                    + "     \"descripcionTipoVia\": \"Transversal\",\n"
                    + "     \"nomenclaturaDireccion\": \"E\",\n"
                    + "     \"valorNumeroNomenclatura\": 4,\n"
                    + "     \"numeroInicialDireccion\": 6,\n"
                    + "     \"numeroFinalDireccion\": 10,\n"
                    + "     \"barrio\": \"Los almendros\",\n"
                    + "     \"descripcionTipoInmueble\": \"Bodega\",\n"
                    + "     \"otroValorDescTipoInmueble\": \"\",\n"
                    + "     \"descripcionComplemento\": \"Bloque\",\n"
                    + "     \"otroValorDescComplemento\": \"\",\n"
                    + "     \"detalles\": \"\"\n"
                    + "},\n"
                    +"    \"idActividadEconomica\": 1,\n"
                    +"    \"idPaisSede\": 1,\n"
                    +"    \"idMunicipioSede\": 1,\n"
                    +"    \"idDepartamentoSede\": 1,\n"
                    +"    \"telefonoSede\": \"12345\",\n"
                    + "    \"numeroDocumento\": \"12345\",\n"
                    +"    \"codigoInscripcion\": \"12345\"\n"
                    +"}" +
                    "<br>", required = true) String json) {
        return service.crearSedeEmpresa(json);
    }

    @Override
    @PutMapping("/actualizarDireccion")
    @ApiOperation(value = "Actualizar direcciòn", notes = "Actualizar direcciòn")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. El recurso se obtiene correctamente", response = GenericResponseDTO.class),
            @ApiResponse(code = 400, message = "Bad Request.Esta vez cambiamos el tipo de dato de la respuesta (String)", response = String.class),
            @ApiResponse(code = 500, message = "Error inesperado del sistema")})
    public ResponseEntity<GenericResponseDTO> actualizarDireccion(@ApiParam(type = "Json", value =
            "el parametro direcciòn  debe ser un json con la siguiente estructura:" +
                    "<br>"
                    + " { <br>"
                    + "     \"idSede\": 66,\n"
                    + "     \"estructuraDireccionJudicial\": "
                    + "{\n"
                    + "     \"descripcionTipoDireccion\": \"Urbana\",\n"
                    + "     \"descripcionTipoVia\": \"Calle\",\n"
                    + "     \"nomenclaturaDireccion\": \"B\",\n"
                    + "     \"valorNumeroNomenclatura\": 3,\n"
                    + "     \"numeroInicialDireccion\": 5,\n"
                    + "     \"numeroFinalDireccion\": 70,\n"
                    + "     \"barrio\": \"Los almendros\",\n"
                    + "     \"descripcionTipoInmueble\": \"Parque empresarial\",\n"
                    + "     \"otroValorDescTipoInmueble\": \"\",\n"
                    + "     \"descripcionComplemento\": \"Lote\",\n"
                    + "     \"otroValorDescComplemento\": \"\",\n"
                    + "     \"detalles\": \"\"\n"
                    + "},\n"
                    + "     \"estructuraDireccionNotificacion\": "
                    + "{\n"
                    + "     \"descripcionTipoDireccion\": \"Urbana\",\n"
                    + "     \"descripcionTipoVia\": \"Carrera\",\n"
                    + "     \"nomenclaturaDireccion\": \"E\",\n"
                    + "     \"valorNumeroNomenclatura\": 4,\n"
                    + "     \"numeroInicialDireccion\": 6,\n"
                    + "     \"numeroFinalDireccion\": 10,\n"
                    + "     \"barrio\": \"Los almendros\",\n"
                    + "     \"descripcionTipoInmueble\": \"Complejo industrial\",\n"
                    + "     \"otroValorDescTipoInmueble\": \"\",\n"
                    + "     \"descripcionComplemento\": \"Manzana\",\n"
                    + "     \"otroValorDescComplemento\": \"\",\n"
                    + "     \"detalles\": \"\"\n"
                    + "},\n"
                    + "     \"estructuraDireccionComercial\": "
                    + "{\n"
                    + "     \"descripcionTipoDireccion\": \"Urbana\",\n"
                    + "     \"descripcionTipoVia\": \"Transversal\",\n"
                    + "     \"nomenclaturaDireccion\": \"E\",\n"
                    + "     \"valorNumeroNomenclatura\": 4,\n"
                    + "     \"numeroInicialDireccion\": 6,\n"
                    + "     \"numeroFinalDireccion\": 10,\n"
                    + "     \"barrio\": \"Los almendros\",\n"
                    + "     \"descripcionTipoInmueble\": \"Bodega\",\n"
                    + "     \"otroValorDescTipoInmueble\": \"\",\n"
                    + "     \"descripcionComplemento\": \"Bloque\",\n"
                    + "     \"otroValorDescComplemento\": \"\",\n"
                    + "     \"detalles\": \"\"\n"
                    + "}\n"
                    + "      }<br>", required = true) String genericRequestDTO) {
        return service.actualizarDireccion(genericRequestDTO);
    }


    @Override
    @GetMapping("/popUpConsultarSedeEmpresa/{idSede}")
    @ApiOperation(value = "Consulta sede empresa", notes = "Consulta sede empresa")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. El recurso se obtiene correctamente", response = GenericResponseDTO.class),
            @ApiResponse(code = 400, message = "Bad Request.Esta vez cambiamos el tipo de dato de la respuesta (String)", response = String.class),
            @ApiResponse(code = 500, message = "Error inesperado del sistema")})
    public ResponseEntity<GenericResponseDTO> popUpConsultarSedeEmpresa(@ApiParam(type = "Integer", value =
            "el parametro usuario debe ser un json con la siguiente estructura:" +
                    "<br>" +
                    "{ <br> request:"
                    + " {<br>"
                    + "     \"id\": \"\"<br>" +
                    "}<br>"

                    + "      }<br>", example = "1", required = true) @PathVariable Integer idSede) {
        return service.consultarSedeEmpresa(idSede);
    }

}
