package co.com.invima.maestro.service.srvSedeEmpresa.web.api.rest.v1;


import co.com.invima.maestro.modeloTransversal.dto.generic.GenericRequestDTO;
import co.com.invima.maestro.modeloTransversal.dto.generic.GenericResponseDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

public interface IEmpresaSedeController {



    ResponseEntity<GenericResponseDTO> consultarPorId(@PathVariable Integer id);

    ResponseEntity<GenericResponseDTO> eliminadoLogicoPorId(@PathVariable Integer id);

    ResponseEntity<GenericResponseDTO> consultarEmpresaSedes();

    ResponseEntity<GenericResponseDTO> crear(@RequestBody GenericRequestDTO genericRequestDTO);

    ResponseEntity<GenericResponseDTO> actualizar(@RequestBody GenericRequestDTO genericRequestDTO);

    ResponseEntity<GenericResponseDTO> popUpEditarSedeEmpresa(@RequestBody String json);

    ResponseEntity<GenericResponseDTO> popUpCrearSede(@RequestBody String json);

    ResponseEntity<GenericResponseDTO> actualizarDireccion(@RequestBody String genericRequestDTO);

    ResponseEntity<GenericResponseDTO> popUpConsultarSedeEmpresa(@RequestBody Integer idSede);
}
