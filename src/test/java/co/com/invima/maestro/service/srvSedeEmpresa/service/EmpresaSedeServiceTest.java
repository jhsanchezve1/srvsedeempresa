package co.com.invima.maestro.service.srvSedeEmpresa.service;

import co.com.invima.maestro.modeloTransversal.dto.generic.GenericRequestDTO;
import co.com.invima.maestro.modeloTransversal.dto.generic.GenericResponseDTO;
import co.com.invima.maestro.modeloTransversal.dto.sedeEmpresa.EmpresaSedeDTO;
import co.com.invima.maestro.service.srvSedeEmpresa.commons.ConfiguradorSpring;
import co.com.invima.maestro.service.srvSedeEmpresa.commons.converter.SedeEmpresaConverter;
import co.com.invima.maestro.service.srvSedeEmpresa.web.api.rest.v1.EmpresaSedeController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ConfiguradorSpring.class})
public class EmpresaSedeServiceTest {

    @Autowired
    EmpresaSedeController empresaSedeController;

    @Autowired
    SedeEmpresaConverter sedeEmpresaConverter;


    @Autowired
    private ModelMapper modelMapper;

    @Test
    public void consultarEmpresaSedes() {
        System.out.println("entro test");
        List<EmpresaSedeDTO> empresaSedeDTOS = new ArrayList<>();
        try {
            ResponseEntity<GenericResponseDTO> response = empresaSedeController.consultarEmpresaSedes();
            System.out.println("Respuesta BD" + response);
            List<Object> lista = (List<Object>) response.getBody().getObjectResponse();
            for (Object empresaSede : lista) {
                EmpresaSedeDTO empresaSedeDTO = new EmpresaSedeDTO();
                modelMapper.map(empresaSede, empresaSedeDTO);
                empresaSedeDTOS.add(empresaSedeDTO);
            }
            assertEquals(empresaSedeDTOS.size(), 1);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Test
    public void consultarPorId() {
        Integer id = 1;
        EmpresaSedeDTO empresaSedeDTO = new EmpresaSedeDTO();
        try {
            ResponseEntity<GenericResponseDTO> response = empresaSedeController.consultarPorId(id);
            Object respuesta = response.getBody().getObjectResponse();
            modelMapper.map(respuesta, empresaSedeDTO);
            assertEquals(empresaSedeDTO.getId(), id);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void crear() {
        GenericRequestDTO genericRequestDTO = new GenericRequestDTO();
        try {
            EmpresaSedeDTO empresaSedeDTO = new EmpresaSedeDTO();
            empresaSedeDTO.setCodigoInscripcion("123");
            empresaSedeDTO.setNombreSede("prueba");
            empresaSedeDTO.setIdPaisSede(1);
            empresaSedeDTO.setIdDepartamentoSede(1);
            empresaSedeDTO.setIdMunicipioSede(2);
            empresaSedeDTO.setTelefonoSede("3046541978");
            empresaSedeDTO.setCelularSede("3046541978");
            empresaSedeDTO.setCorreoElectronicoSede("bermeo1@soaint.com");


            genericRequestDTO.setRequest(empresaSedeDTO);
            ResponseEntity<GenericResponseDTO> response = empresaSedeController.crear(genericRequestDTO);
            Object respuesta = response.getBody().getObjectResponse();

            assertEquals(respuesta, false);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Test
    public void actualizar() {
        GenericRequestDTO genericRequestDTO = new GenericRequestDTO();
        try {
            EmpresaSedeDTO empresaSedeDTO = new EmpresaSedeDTO();
            empresaSedeDTO.setId(21);
            empresaSedeDTO.setCodigoInscripcion("123");
            empresaSedeDTO.setNombreSede("prueba act");
            empresaSedeDTO.setIdPaisSede(1);
            empresaSedeDTO.setIdDepartamentoSede(1);
            empresaSedeDTO.setIdMunicipioSede(2);
            empresaSedeDTO.setTelefonoSede("3046541978");
            empresaSedeDTO.setCelularSede("3046541978");
            empresaSedeDTO.setCorreoElectronicoSede("bermeo1@soaint.com");


            genericRequestDTO.setRequest(empresaSedeDTO);
            ResponseEntity<GenericResponseDTO> response = empresaSedeController.actualizar(genericRequestDTO);
            Object respuesta = response.getBody().getObjectResponse();

            assertEquals(respuesta, false);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void eliminadoLogicoPorId() {
        Integer id = 1;
        try {
            ResponseEntity<GenericResponseDTO> response = empresaSedeController.eliminadoLogicoPorId(id);
            Object respuesta = response.getBody().getObjectResponse();
            assertEquals(respuesta, true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void editarSedeEmpresa() {

        String json = "{\n" +
                "    \"idSede\": 34,\n" +
                "    \"telefonoSede\":\"12345\",\n" +
                "    \"celularSede\": \"12345\",\n" +
                "    \"correoElectronicoSede\": \"sede2@gmail.com\",\n" +
                "    \"idDireccion\": 1,\n" +
                "    \"idActividadEconomica\": 1,\n" +
                "}";
        try {
            ResponseEntity<GenericResponseDTO> response = empresaSedeController.popUpEditarSedeEmpresa(json);
            Object respuesta = response.getBody().getObjectResponse();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void crearSedeEmpresa() {
        String json = "{\n" +

        "    \"idEmpresa\": 146,\n" +
        "    \"nombreSede\":\"prueba\",\n" +
        "    \"celularSede\": \"12345\",\n" +
        "    \"correoElectronicoSede\": \"sede@gmail.com\",\n" +
        "    \"idDireccion\": 1,\n" +
        "    \"idActividadEconomica\": 1,\n" +
        "    \"idMunicipioSede\": 1,\n" +
        "    \"idDepartamentoSede\": 1,\n" +
        "    \"telefonoSede\": \"12345\",\n" +
        "    \"numeroDocumento\": \"12345\",\n" +
        "    \"codigoInscripcion\": \"12345\"\n" +
        "}";
        try {
            ResponseEntity<GenericResponseDTO> response = empresaSedeController.popUpCrearSede(json);
            Object respuesta = response.getBody().getObjectResponse();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
